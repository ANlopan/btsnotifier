const ga = require("golos-addons");
const global = ga.global;
ga.global.initApp("btsnotifier");

const bts = require("./bts");

const log = ga.global.getLogger("btsnotifier");

const CONFIG = global.CONFIG;
const BOTS = CONFIG.bots;

const MIN_ORDER = 1;

let WORKING = true;
let PREV_PRICES = null;

let OID = 1;

let ASSETS = [];


async function checkFilledOrders() {
    for(let bot of BOTS) {
        let orders = await bts.getFilledOrders(bot);
        for(let order of orders) {
            const ba = order.base_amount / Math.pow(10, bot.base.precision);
            const qa = order.quote_amount / Math.pow(10, bot.quote.precision);
            log.info(bot.name, "filled order", order.op, ba.toFixed(bot.base.precision), qa.toFixed(bot.quote.precision), (qa / ba).toFixed(6));    
        }
        //log.debug("filled orders", JSON.stringify(orders, null, 4));
    }
}

async function cancelBotOrders(bot) {
    //log.debug("bot ", bot);
    while(bot.bids.length > 0) {
        let order = bot.bids.pop();
        log.info(bot.name, "canceling bid ", "at price", order.price.toFixed(6));
        await bts.cancelOrder(bot, order.id);
    }
    while(bot.asks.length > 0) {
        let order = bot.asks.pop();
        log.info(bot.name, "canceling ask ", "at price", order.price.toFixed(6));
        await bts.cancelOrder(bot, order.id);
    }
}

async function cancelAllOrders() {
    for(let bot of BOTS) {
        await cancelBotOrders(bot);
    }
}


async function getInfos() {
    
    const balance = await bts.getBalance();
    const orderBook = await getOrderBook();
    let bid = parseFloat(orderBook.bids[0].real_price);
    let ask = parseFloat(orderBook.asks[0].real_price);    
    
    log.debug("current bid = " + bid.toFixed(6));
    log.debug("current ask = " + ask.toFixed(6));

    const prices = await calculateDesiredPrices(bid, ask);    

    return {
        balance : balance,
        top_prices : { bid : bid, ask : ask },
        prices : prices
    };
}



async function processBot(bot) {
    await bts.getUpdateBotInfos(bot);
    log.trace("processing " + bot.name);

    log.trace(bot.market);
    log.trace(bot.bids);

    const market_top_bid = bot.market.bids[0].price;
    const market_top_ask = bot.market.asks[0].price;

    const desired_bid_price = market_top_bid / (1 + bot.base.percent / 100);
    const desired_ask_price = market_top_ask * (1 + bot.quote.percent / 100);
    
    let mybid = bot.bids.length > 0?bot.bids[0]:null;
    let myask = bot.asks.length > 0?bot.asks[0]:null;
    
    if(mybid && mybid.price < desired_bid_price - desired_bid_price * 0.002) {
        log.debug(bot.name,"mybid.price", mybid.price, "desired_bid_price",desired_bid_price);
        await bts.cancelOrder(bot, bot.bids[0].id);
    }

    if(myask && myask.price > desired_ask_price + desired_ask_price * 0.002) {
        log.debug(bot.name,"myask.price", myask.price, "desired_ask_price",desired_ask_price);
        await bts.cancelOrder(bot, bot.asks[0].id);
    }

    if(bot.bids.length == 0) {
        log.debug(bot.name,"make a bid at " + desired_bid_price);
        await bts.createOrder(bot, bot.quote, bot.base, desired_bid_price);
    }

    if(bot.asks.length == 0) {
        log.debug(bot.name,"make an ask at " + desired_ask_price);
        await bts.createOrder(bot, bot.base, bot.quote, desired_ask_price);
    }
}

async function updateOrders() {
    log.trace("updateOrders");
    await checkFilledOrders();

    if(WORKING) {
        for(let bot of BOTS) {
            await processBot(bot);
        }
    }

}

function checkAssets(ass1, ass2) {
    for(let bot of BOTS) {
        if(bot.base.asset_id == ass1 && bot.quote.asset_id == ass2) {
            return true;
        }
        if(bot.base.asset_id == ass2 && bot.quote.asset_id == ass1) {
            return true;
        }
    }
    return false;
}

async function processBlock(bn) {
    log.trace("processing block " + bn);
    let block = await bts.getBlock(bn);
    let found_order_changes = false;
    for(let tr of block.transactions) {
        //log.debug("tr", JSON.stringify(tr, null, 4));
        for(let rawOp of tr.operations) {
            let op = rawOp[0];
            let opBody = rawOp[1];
            log.trace("op", op);
            switch(op) {
                case 1: //limit_order_create_operation
                    found_order_changes = (!checkAssets(opBody.amount_to_sell.asset_id, opBody.min_to_receive.asset_id));
                    break;
            }
        }
    }             
    if(found_order_changes)  {
        await updateOrders();
    }
}

async function updateBots() {
    for(let bot of BOTS) {
        await bts.queryIDs(bot);
        await bts.getUpdateBotInfos(bot);
        if(!ASSETS.includes(bot.base.asset_id)) {
            ASSETS.push(bot.base.asset_id);
        }
        if(!ASSETS.includes(bot.quote.asset_id)) {
            ASSETS.push(bot.quote.asset_id);
        }
    }    
    log.debug("ASSETS", ASSETS);
}

async function run() {

    await bts.init();
    await updateBots();
    await cancelAllOrders();


    let res = await bts.getFilledOrders(BOTS[0]);
    //process.exit();

    let props = await bts.getProps();
    let currentBlock = props.head_block_number;

    //Start clean
    //await closeExistingOrders();
    //start working
    //await updateOrders();

    while(true) {
        try {
            props = await bts.getProps();

            if(props.head_block_number < currentBlock) {
                //log.info(`no new blocks, skip round`);
                await ga.global.sleep(1000*6);
                continue;
            }

            await processBlock(currentBlock++);

        } catch(e) {
            log.error("Error catched in main loop!");
            log.error(e);
        }  
    } 
    process.exit(1);
}


run();



